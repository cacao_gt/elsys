<?php
	include_once 'conexion.php';

	if(isset($_GET['idrp'])){
		$id=(int) $_GET['idrp'];

		$buscar_id=$con->prepare('SELECT * FROM roles_permisos WHERE idrp=:id LIMIT 1');
		$buscar_id->execute(array(
			':id'=>$id
		));
		$resultado=$buscar_id->fetch();
	}else{
		header('Location: roles_permisos.php');
	}


	if(isset($_POST['guardar'])){
		$idrol= $_POST["idrol"];	
	    $idpermiso= $_POST["idpermiso"];
		$id=(int) $_GET['idrp'];

		if(empty($idrol) && empty($idpermiso) ){
			}else{
				$consulta_update=$con->prepare(' UPDATE roles_permisos SET  
					idrol=:idrol,
					idpermiso=:idpermiso
					WHERE idrp=:id;'
				);
				$consulta_update->execute(array(
					':idrol' =>$idrol,
					':idpermiso' =>$idpermiso,
					':id' =>$id
				));
				header('Location: roles_permisos.php');
			}
		}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/estilo.css">
</head>
<body>
	<div class="contenedor">
		<h2>ROLES - PERMISOS</h2>
		<form action="" method="post">
			<div class="form-group">
	<input type="number" name="idrol" value="<?php if($resultado) echo $resultado['idrol']; ?>" class="input__text">
	<input type="number" name="idpermiso" value="<?php if($resultado) echo $resultado['idpermiso']; ?>" class="input__text">
			</div>
			<div class="form-group">
			</div>
			
			<div class="btn__group">
				<a href="roles_permisos.php" class="btn btn__danger">Cancelar</a>
				<input type="submit" name="guardar" value="Guardar" class="btn btn__primary">
			</div>
		</form>
	</div>
</body>
</html>