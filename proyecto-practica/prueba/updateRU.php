<?php
	include_once 'conexion.php';

	if(isset($_GET['idru'])){
		$id=(int) $_GET['idru'];

		$buscar_id=$con->prepare('SELECT * FROM roles_usuarios WHERE idru=:id LIMIT 1');
		$buscar_id->execute(array(
			':id'=>$id
		));
		$resultado=$buscar_id->fetch();
	}else{
		header('Location: roles_usuarios.php');
	}


	if(isset($_POST['guardar'])){
		$idrol= $_POST["idrol"];	
	    $idusuario= $_POST["idusuario"];
		$id=(int) $_GET['idru'];

		if(empty($idrol) && empty($idusuario) ){
			}else{
				$consulta_update=$con->prepare(' UPDATE roles_usuarios SET  
					idrol=:idrol,
					idusuario=:idusuario
					WHERE idru=:id;'
				);
				$consulta_update->execute(array(
					':idrol' =>$idrol,
					':idusuario' =>$idusuario,
					':id' =>$id
				));
				header('Location: roles_usuarios.php');
			}
		}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/estilo.css">
</head>
<body>
	<div class="contenedor">
		<h2>ROLES - USUARIOS</h2>
		<form action="" method="post">
			<div class="form-group">
	<input type="number" name="idrol" value="<?php if($resultado) echo $resultado['idrol']; ?>" class="input__text">
	<input type="number" name="idusuario" value="<?php if($resultado) echo $resultado['idusuario']; ?>" class="input__text">
			</div>
			<div class="form-group">
			</div>
			
			<div class="btn__group">
				<a href="roles_usuarios.php" class="btn btn__danger">Cancelar</a>
				<input type="submit" name="guardar" value="Guardar" class="btn btn__primary">
			</div>
		</form>
	</div>
</body>
</html>